#!/bin/bash
set -e
cd /var/www/eviction-defense-tool
git pull
composer install --no-dev --no-progress
cd web
drush updb --yes
# For now, assume this instance has the correct config and do not import.
# drush config:import
