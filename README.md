# Eviction defense tool

To install a dev instance, run:

```
cp .env.example .env
```

Update the "ChangeMe" variables to use random strings.

```
ln -s docker-compose.develop.yml docker-compose.yml
docker-compose up -d
docker-compose exec --user www-data app drush site:install --existing-config
```

To update the configuration of an existing dev instance:

```
docker-compose up --build --force-recreate -d
docker-compose exec --user www-data app drush config:import
```

Note: Most of the time you only need to recreate the app container,
but the above command recreates all containers.

To clear all caches:

```
docker-compose exec --user www-data app drush cr
```

Note: You could save time in development by setting
DRUPAL_SKIP_CSS_AGGREGATION=1 in your .env file.
