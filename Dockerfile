FROM ubuntu:latest
RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    composer \
    libapache2-mod-php \
    mysql-client \
    php-apcu \
    php-curl \
    php-gd \
    php-mysql \
    php-xml \
  && rm -rf /var/lib/apt/lists/* \
  && a2enmod expires \
  && a2enmod rewrite
COPY ["composer.json", "composer.lock", "/var/www/"]
WORKDIR /var/www
RUN composer install --no-progress
COPY ["web/", "/var/www/web/"]
COPY ["config/", "/var/www/config/"]
RUN mkdir /var/www/private /var/www/tmp /var/www/web/files /var/www/translations \
  && chown -R www-data:www-data /var/www/private /var/www/tmp /var/www/web/files /var/www/config/sync /var/www/translations
WORKDIR /var/www/web
RUN ln -s /var/www/vendor/bin/drush /usr/local/bin/drush
COPY ["docker/site.conf", "/etc/apache2/sites-available/000-default.conf"]
COPY ["docker/docker-entrypoint", "/usr/local/bin/docker-entrypoint"]
COPY ["docker/apache2-foreground", "/usr/local/bin/apache2-foreground"]
RUN ln -sfT /dev/stderr /var/log/apache2/error.log
ENTRYPOINT ["docker-entrypoint"]
VOLUME /var/www/web/files /var/www/private /var/www/tmp /var/www/translations
EXPOSE 80
STOPSIGNAL SIGWINCH
CMD ["apache2-foreground"]
